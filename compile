#!/bin/sh

DOS2UNIX=NO
FULLREBUILD=NO
VERSIONPKG="4.0.1.RU.`date +%d%m%Y`"

#---Set_env_ROOTDIR_to_current_dir---#
ROOTDIR=`pwd`
export ROOTDIR=$ROOTDIR
export FIRMROOT=$ROOTDIR
export LINUXDIR=$ROOTDIR/linux

#-------Set_env_LANG------#
export LANG=en_US
export LC_ALL=POSIX

#----------MODE------------#
MODE="$1"
case "$MODE" in
	####################################
	# dual band 7621A + 7602NE + 7612NE
	####################################
	"MT7621-MT7602-MT7612-2T2R-16M-USB")
	    # metal sample
	    DEVNAME="SNR-CPE-ME2-5GHZ-USB-MT"
	    ;;
	"MT7621-MT7602-MT7612-2T2R-8M-USB")
	    # tube sample
	    DEVNAME="SNR-CPE-ME2-5GHZ-USB-MT"
	    ;;
	####################################
	# dual band 7620A + 7612NE
	####################################
	"MT7620-2T2R-MT7612-2T2R-5GHZ-16M-USB")
	    # metal horizont sample
	    DEVNAME="SNR-CPE-MD2-5GHZ-MT"
	    ;;
	"MT7620-2T2R-EXTPA-EXTLNA-MT7612-2T2R-5GHZ-8M")
	    # black horizont sample
	    DEVNAME="SNR-CPE-MD2-5GHZ-MT"
	    ;;
	####################################
	# dual band 7620A + 7610NE
	####################################
	"MT7620-2T2R-MT7610-1T1R-5GHZ-16M-USB")
	    # white sample
	    DEVNAME="SNR-CPE-MD1-5GHZ-USB-MT"
	    ;;
	"MT7620-2T2R-EXTPA-EXTLNA-MT7610-1T1R-5GHZ-16M-USB")
	    # black sample
	    DEVNAME="SNR-CPE-MD1-5GHZ-USB-MT"
	    ;;
	"MT7620-2T2R-INTPA-EXTLNA-MT7610-1T1R-5GHZ-16M-USB")
	    # none sample
	    DEVNAME="SNR-CPE-MD1-5GHZ-USB-MT"
	    ;;
	"MT7620-2T2R-MT7610-1T1R-5GHZ-8M")
	    # black dualband release
	    DEVNAME="SNR-CPE-MD1-5GHZ-MT"
	    ;;
	####################################
	# one band 7620N
	####################################
	"MT7620-2T2R-8M")
	    # white oneband release
	    DEVNAME="SNR-CPE-W4N-MT"
	    ;;
	*)
	    echo "Not supported (unknown) mode or mode not set."
	    exit 1
esac

#-------FABRICMODE---------#
if [ "$2" = "YES" ]; then
    FMODE="YES"
fi

#-------OLDCMODE---------#
if [ "$3" = "YES" ]; then
    # old drivers config path
    KCOPATH="$ROOTDIR/configs/kernel_old_drv"
else
    # current drivers config path
    KCOPATH="$ROOTDIR/configs/kernel"
fi

# use for product id and model name
export REALNAME="$MODE"
export PRODUCTID="$DEVNAME"
export MODEL_NAME="$DEVNAME"
export KERNEL_VER="$VERSIONPKG"

echo "_________________________________________________________"
echo "> Building for $PRODUCTID version $KERNEL_VER <"
echo "_________________________________________________________"

echo ""								>  $ROOTDIR/sdk_version.h
echo "#define RT288X_SDK_VERSION \"$DEVNAME-$VERSIONPKG\""	>> $ROOTDIR/sdk_version.h
echo "#define DEVNAME  \"$DEVNAME\""				>> $ROOTDIR/sdk_version.h
echo "#define REALNAME \"$MODE\""				>> $ROOTDIR/sdk_version.h
echo "#define VERSIONPKG  \"$VERSIONPKG\""			>> $ROOTDIR/sdk_version.h
echo "#define VERSIONSTR \"$DEVNAME-$VERSIONPKG\""		>> $ROOTDIR/sdk_version.h
echo ""								>> $ROOTDIR/sdk_version.h
echo ""								>  $ROOTDIR/version
echo "RT288X_SDK_VERSION = \"$VERSIONPKG\""			>> $ROOTDIR/version
echo "DEVNAME = \"$DEVNAME\""					>> $ROOTDIR/version
echo "VERSIONPKG = \"$VERSIONPKG\""				>> $ROOTDIR/version
echo "VERSIONSTR = \"$DEVNAME-$VERSIONPKG\""			>> $ROOTDIR/version
echo ""								>> $ROOTDIR/version

#for clear all binary
if [ "$FULLREBUILD" = "YES" ] ; then
echo -------------------------------FULL-CLEAR-------------------------------
    ./clear
fi

rm -rfv $ROOTDIR/romfs/*
rm -rfv $ROOTDIR/images/*
ln -sf  $ROOTDIR/linux-3.4.x $ROOTDIR/linux

echo --------------------------------COPY-CONFIG-----------------------------
#####################################################PER-DEVICES-CONFIGS################################################################
if [ "$MODE" = "MT7621-MT7602-MT7612-2T2R-16M-USB" ]; then
    cp -fv $KCOPATH/config-kernel-MT7621-MT7602-MT7612-2T2R-16M-USB			$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox-USB					$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config-16M-USB					$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7621-MT7602-MT7612-2T2R-8M-USB" ]; then
    cp -fv $KCOPATH/config-kernel-MT7621-MT7602-MT7612-2T2R-8M-USB			$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox-USB					$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config-USB					$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-EXTPA-EXTLNA-MT7610-1T1R-5GHZ-16M-USB" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-EXTPA-EXTLNA-MT7610-1T1R-5GHZ-16M-USB	$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox-USB					$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config-16M-USB					$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-INTPA-EXTLNA-MT7610-1T1R-5GHZ-16M-USB" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-INTPA-EXTLNA-MT7610-1T1R-5GHZ-16M-USB	$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox-USB					$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config-16M-USB					$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-MT7610-1T1R-5GHZ-16M-USB" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-MT7610-1T1R-5GHZ-16M-USB			$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox-USB					$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config-16M-USB					$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-EXTPA-EXTLNA-MT7612-2T2R-5GHZ-8M" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-EXTPA-EXTLNA-MT7612-2T2R-5GHZ-8M		$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox						$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config						$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-MT7612-2T2R-5GHZ-16M-USB" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-MT7612-2T2R-5GHZ-16M-USB			$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox-USB					$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config-16M-USB					$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-MT7610-1T1R-5GHZ-8M" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-MT7610-1T1R-5GHZ-8M			$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox						$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config						$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
elif [ "$MODE" = "MT7620-2T2R-8M" ]; then
    cp -fv $KCOPATH/config-kernel-MT7620-2T2R-8M					$ROOTDIR/linux/.config
    cp -fv $ROOTDIR/configs/all/config-busybox						$ROOTDIR/user/busybox/.config
    cp -fv $ROOTDIR/configs/all/config-config						$ROOTDIR/config/.config
    cp -fv $ROOTDIR/configs/all/config-lib						$ROOTDIR/lib/.config
else
    echo "Config files not found."
    exit 1
fi
#####################################################DIFF-MODES###########################################################################
# enable ATE and others support for fabric
if [ "$FMODE" = "YES" ]; then
    echo "BUILD FABRIC MODE"

    echo "Copy ATE code to tree"
    mkdir -p $ROOTDIR/user/rt2880_app/ated
    cp -rf /mnt/svalka/linux/ate/ated/* $ROOTDIR/user/rt2880_app/ated
    mkdir -p $ROOTDIR/linux-3.4.x/drivers/net/wireless/ralink/rt3xxx/rt2860v2/ate
    cp -rf /mnt/svalka/linux/ate/ate/rt3xxx/rt2860v2/ate/* $ROOTDIR/linux-3.4.x/drivers/net/wireless/ralink/rt3xxx/rt2860v2/ate
    mkdir -p $ROOTDIR/linux-3.4.x/drivers/net/wireless/ralink/rtpci/mt7610/ate
    cp -rf /mnt/svalka/linux/ate/ate/rtpci/mt7610/ate/* $ROOTDIR/linux-3.4.x/drivers/net/wireless/ralink/rtpci/mt7610/ate
    mkdir -p $ROOTDIR/linux-3.4.x/drivers/net/wireless/ralink/rtpci/mt76x2/ate
    cp -rf /mnt/svalka/linux/ate/ate/rtpci/mt76x2/ate/* $ROOTDIR/linux-3.4.x/drivers/net/wireless/ralink/rtpci/mt76x2/ate

    echo "Reconfigure kernel"
    sed 's/# CONFIG_RT2860V2_AP_ATE is not set/CONFIG_RT2860V2_AP_ATE=y/g' $ROOTDIR/linux/.config > $ROOTDIR/linux/.config.tmp
    mv -f $ROOTDIR/linux/.config.tmp $ROOTDIR/linux/.config
    sed 's/# CONFIG_MT7610_AP_ATE is not set/CONFIG_MT7610_AP_ATE=y/g' $ROOTDIR/linux/.config > $ROOTDIR/linux/.config.tmp
    mv -f $ROOTDIR/linux/.config.tmp $ROOTDIR/linux/.config
    sed 's/# CONFIG_MT76X2_AP_ATE is not set/CONFIG_MT76X2_AP_ATE=y/g' $ROOTDIR/linux/.config > $ROOTDIR/linux/.config.tmp
    mv -f $ROOTDIR/linux/.config.tmp $ROOTDIR/linux/.config

    echo "Reconfigure userspace"
    sed 's/# CONFIG_TELNETD is not set/CONFIG_TELNETD=y/g' $ROOTDIR/user/busybox/.config > $ROOTDIR/user/busybox/.config.tmp
    mv -f $ROOTDIR/user/busybox/.config.tmp $ROOTDIR/user/busybox/.config
    sed 's/# CONFIG_FEATURE_TELNETD_INETD_WAIT is not set/CONFIG_FEATURE_TELNETD_INETD_WAIT=y/g' $ROOTDIR/user/busybox/.config > $ROOTDIR/user/busybox/.config.tmp
    mv -f $ROOTDIR/user/busybox/.config.tmp $ROOTDIR/user/busybox/.config
    sed 's/# CONFIG_RALINKAPP_ATED is not set/CONFIG_RALINKAPP_ATED=y/g' $ROOTDIR/config/.config > $ROOTDIR/config/.config.tmp
    mv -f $ROOTDIR/config/.config.tmp $ROOTDIR/config/.config
fi

#for wantusoid`s like.
if [ "$DOS2UNIX" = "YES" ] ; then
echo ---------------------------------DOS2UNIX--------------------------------
    find -type f -exec dos2unix {} \;
fi

echo -------------------------------MAKE-OLDCONFIGS---------------------------
make oldconfig
make dep

echo ---------------------------------MAKE-ALL--------------------------------
make

if [ -f Uboot/uboot.bin ] && [ -f images/zImage.lzma ] ; then
echo -------------------------------MAKE-FULLDUMP-----------------------------
    make -C fulldump
fi

echo -----------------------------------PACK----------------------------------
mv -f images/*.bin "images/$DEVNAME-$MODE.$VERSIONPKG.bin"
zip -r images/$DEVNAME-$MODE.$VERSIONPKG.bin.zip images/*.bin

echo ---------------------------------END BUILD-------------------------------
