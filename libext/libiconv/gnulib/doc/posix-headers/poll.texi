@node poll.h
@section @file{poll.h}

POSIX specification:@* @url{http://www.opengroup.org/onlinepubs/9699919799/basedefs/poll.h.html}

Gnulib module: poll-h

Portability problems fixed by Gnulib:
@itemize
@item
This header file is missing on some platforms:
mingw, BeOS.
@end itemize

Portability problems not fixed by Gnulib:
@itemize
@end itemize
