@node sys/types.h
@section @file{sys/types.h}

POSIX specification:@* @url{http://www.opengroup.org/onlinepubs/9699919799/basedefs/sys_types.h.html}

Gnulib module: ---

Portability problems fixed by Gnulib:
@itemize
@end itemize

Portability problems not fixed by Gnulib:
@itemize
@end itemize
