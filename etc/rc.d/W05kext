#!/bin/sh

# get params
. /etc/scripts/global.sh

LOG="logger -t kext"

start() {
    six_mode
    nat_mode
    offload
    smb_fp
    pthrough
    # this work only if bridge used
    if [ "$lan_if" = "br0" ]; then
	multicast
	# do not isolate in full bridge mode, this break data transfer
	if [ "$switchpart" != "LLLLL" ]; then
	    lanisolate
	fi
	bridge_stp
    fi
}

six_mode() {
    if [ "$IPv6OpMode" != "0" ]; then
	sysctl -wq net.ipv6.conf.all.disable_ipv6=0
	sysctl -wq net.ipv6.conf.default.disable_ipv6=0
    else
	sysctl -wq net.ipv6.conf.all.disable_ipv6=1
	sysctl -wq net.ipv6.conf.default.disable_ipv6=0
    fi
}

nat_mode() {
    eval `nvram_buf_get 2860 nat_mode`
    if [ "$nat_mode" = "1" ]; then
	sysctl -wq net.netfilter.nf_conntrack_nat_mode=1
	$LOG "Nat mode Full Cone"
    elif [ "$nat_mode" = "2" ]; then
	sysctl -wq net.netfilter.nf_conntrack_nat_mode=2
	$LOG "Nat mode Restricted Cone"
    else
	sysctl -wq net.netfilter.nf_conntrack_nat_mode=0
	$LOG "Nat mode Linux Hybrid"
    fi
}

smb_fp() {
    if [ "$CONFIG_NETFILTER_FP_SMB" = "y" ]; then
	eval `nvram_buf_get 2860 smbFastpath`
	if [ "$smbFastpath" = "1" ] && [ "$lan_ipaddr" != "" ]; then
	    $LOG "SMB traffic offload enabled on $lan_ipaddr"
	    echo "$lan_ipaddr" > /proc/net/netfilter/nf_fp_smb
	else
	    $LOG "SMB traffic offload disabled"
	    echo "0.0.0.0" > /proc/net/netfilter/nf_fp_smb
	fi
    fi
}

offload() {
    # In bridge mode not fastpaths or hw_nat use
    if [ "$OperationMode" != "0" ] && [ "$ApCliBridgeOnly" != "1" ]; then
	eval `nvram_buf_get 2860 hw_nat_bind hw_nat_wifi hw_nat_udp hw_nat_six offloadMode natFastpath routeFastpath filterFastpath store_ttl nodogsplash_enable`
	# select NAT offload mode
	if [ "$offloadMode" = "1" ]; then
	    hw_nat=0
	    sw_nat=1
	    $LOG "NAT Offload mode software with selected fastpaths, hw_nat disabled."
	elif [ "$offloadMode" = "2" ]; then
	    hw_nat=1
	    sw_nat=0
	    $LOG "NAT Offload mode hardware, all fastpaths disabled."
	elif [ "$offloadMode" = "3" ]; then
	    hw_nat=1
	    sw_nat=1
	    $LOG "NAT Offload mode complex, enable hw_nat and selected software fastpaths."
	else
	    hw_nat=0
	    sw_nat=0
	    $LOG "NAT Offload disabled by user, hw_nat and all fastpaths disabled."
	fi
	if [ "$nodogsplash_enable" = "1" ]; then
	    sw_nat=0
	    $LOG "Nodogsplash enabled  - disable NAT Fastpath."
	fi
	if [ "$CONFIG_BCM_NAT" = "y" ]; then
	    # configure software nat offload
	    if [ "$natFastpath" != "0" ] && [ "$sw_nat" = "1" ]; then
	        $LOG "NAT fastpath enabled."
	        sysctl -wq net.netfilter.nf_conntrack_fastnat=1
	    else
	        sysctl -wq net.netfilter.nf_conntrack_fastnat=0
	    fi
	    # configure software route offload
	    if [ "$routeFastpath" != "0" ] && [ "$sw_nat" = "1" ]; then
		$LOG "Route fastpath enabled."
		sysctl -wq net.netfilter.nf_conntrack_fastroute=1
	    else
		sysctl -wq net.netfilter.nf_conntrack_fastroute=0
	    fi
	fi
	if [ "$CONFIG_IP_NF_IPTABLES_SPEEDUP" = "y" ]; then
	    # configure software netfilter offload
	    if [ "$filterFastpath" != "0" ] && [ "$sw_nat" = "1" ]; then
		$LOG "Netfilter fastpath enabled."
		sysctl -wq net.netfilter.nf_conntrack_skip_filter=1
	    else
		sysctl -wq net.netfilter.nf_conntrack_skip_filter=0
	    fi
	fi
	# configure hardware nat offload
	if [ "$hw_nat" = "1" ]; then
	    # before config unload driver
	    if [ -d /sys/module/hw_nat ]; then
		rmmod hw_nat > /dev/null 2>&1
	    fi
	    # configure hw_nat module
	    if [ "$hw_nat_wifi" = "1" ]; then
	        hw_nat_wifi="wifi_offload=1"
	    else
		hw_nat_wifi="wifi_offload=0"
	    fi
	    if [ "$hw_nat_udp" = "1" ]; then
	        hw_nat_udp="udp_offload=1"
	    else
		hw_nat_udp="udp_offload=0"
	    fi
	    if [ "$CONFIG_RA_HW_NAT_IPV6" != "" ]; then
		if [ "$hw_nat_six" = "1" ] && [ "$IPv6OpMode" != "0" ]; then
		    hw_nat_ipv6="ipv6_offload=1"
		else
    		    hw_nat_ipv6="ipv6_offload=0"
		fi
	    fi
	    if [ "$store_ttl" = "1" ]; then
		hw_nat_ttl="ttl_regen=0"
	    else
		hw_nat_ttl="ttl_regen=1"
	    fi
	    # load ppe driver
	    $LOG "hw_nat: load driver with options $hw_nat_wifi $hw_nat_udp $hw_nat_ipv6 $hw_nat_ttl"
	    modprobe -q hw_nat "$hw_nat_wifi" "$hw_nat_udp" "$hw_nat_ipv6" "$hw_nat_ttl"
	    # reconfigure some options
	    if [ "$hw_nat_bind" != "" ]; then
	        $LOG "hw_nat: set binding threshold to $hw_nat_bind."
	        hw_nat -N $hw_nat_bind > /dev/null 2>&1
	    fi
	else
	    # hw offload disabled
	    if [ -d /sys/module/hw_nat ]; then
		rmmod hw_nat > /dev/null 2>&1
	    fi
	fi
    else
	$LOG "NAT Offload mode not supported in this device mode (bridge/apclibridge/etc), hw_nat and all fastpaths disabled."
	if [ "$CONFIG_BCM_NAT" = "y" ]; then
	    if [ "$CONFIG_BCM_NAT_FASTPATH" = "y" ]; then
		sysctl -wq net.netfilter.nf_conntrack_fastnat=0
	    fi
	    sysctl -wq net.netfilter.nf_conntrack_fastroute=0
	fi
	if [ "$CONFIG_IP_NF_IPTABLES_SPEEDUP" = "y" ]; then
	    sysctl -wq net.netfilter.nf_conntrack_skip_filter=0
	fi
	if [ -d /sys/module/hw_nat ]; then
	    rmmod hw_nat > /dev/null 2>&1
	fi
    fi
}

pthrough() {
    # In bridge and chillispot mode not passth use
    if [ "$OperationMode" != "0" ] && [ "$ApCliBridgeOnly" != "1" ]; then
	eval `nvram_buf_get 2860 pppoe_pass ipv6_pass`
	# pppoe and ip_v6 kernel mode relay.
	if [ -d /proc/net/pthrough ]; then
	    if [ "$pppoe_pass" = "1" ]; then
		$LOG "PPPOE Pass Through enable for $lan_if and $wan_if interfaces."
		echo "$lan_if,$wan_if" > /proc/net/pthrough/pppoe
	    else
		echo "null,null" > /proc/net/pthrough/pppoe
	    fi
	    if [ "$ipv6_pass" = "1" ]; then
		$LOG "IPv6 Pass Through enable for $lan_if and $wan_if interfaces."
		echo "$lan_if,$wan_if" > /proc/net/pthrough/ipv6
	    else
		echo "null,null" > /proc/net/pthrough/ipv6
	    fi
	fi
    else
	if [ -d /proc/net/pthrough ]; then
	    echo "null,null" > /proc/net/pthrough/pppoe
	    echo "null,null" > /proc/net/pthrough/ipv6
	fi
    fi
}

multicast() {
    if [ "$switchpart" = "LLLLL" ]; then
	# for apcli gw/bridge set m2u to eth2 (not used vlan particion in this mode)
    	phys_lan_if="eth2"
    fi
    eval `nvram_buf_get 2860 igmpM2UConvMode igmpFastLeave`
    wlifs=`ip -o -4 link show up | grep -E "ra|wds" | awk {' print $2 '} | cut -f -1 -d :`
    if [ "$igmpM2UConvMode" = "all" ]; then
        INTERFACES="$phys_lan_if $wlifs"
    elif [ "$igmpM2UConvMode" = "lan" ]; then
        INTERFACES="$phys_lan_if"
    elif [ "$igmpM2UConvMode" = "wlan" ]; then
        INTERFACES="$wlifs"
    else
	# disable for all ifaces
	INTERFACES="$phys_lan_if $wlifs"
	for ifname in $INTERFACES; do
	    echo 0 > /sys/devices/virtual/net/$ifname/brport/multicast_fast_leave
	    echo 0 > /sys/devices/virtual/net/$ifname/brport/multicast_to_unicast
	done
	INTERFACES=""
    fi
    if [ "$INTERFACES" != "" ]; then
	$LOG "Enable multicast to unicast conversion for $INTERFACES"
	for ifname in $INTERFACES; do
	    if [ "$igmpFastLeave" = "1" ]; then
		$LOG "Enable bridge igmp fast leave for $INTERFACES"
		echo 1 > /sys/devices/virtual/net/$ifname/brport/multicast_fast_leave
	    else
		echo 0 > /sys/devices/virtual/net/$ifname/brport/multicast_fast_leave
	    fi
	    echo 1 > /sys/devices/virtual/net/$ifname/brport/multicast_to_unicast
	done
    fi
}

lanisolate() {
    eval `nvram_buf_get 2860 LanIsolate NoForwardingBTNBSSID`
    if [ "$LanIsolate" = "1" ]; then
	$LOG "Enable LAN2WIFI isolation mode."
	echo 1 > /sys/devices/virtual/net/$phys_lan_if/brport/isolate_mode
	if [ "$NoForwardingBTNBSSID" = "1" ]; then
	    $LOG "Full MBSSID isolation mode enabled."
	else
	    NoForwardingBTNBSSID="0"
	fi
	wlifs=`ip -o -4 link show up | grep ra | awk {' print $2 '} | cut -f -1 -d :`
	for ifname in $wlifs; do
	    echo "$NoForwardingBTNBSSID" > /sys/devices/virtual/net/$ifname/brport/isolate_mode
	done
    else
	echo 0 > /sys/devices/virtual/net/$phys_lan_if/brport/isolate_mode
    fi
}

bridge_stp() {
    stpEnabled=`nvram_get 2860 stpEnabled`
    if [ "$stpEnabled" = "1" ]; then
	$LOG "Bridge STP enabled."
	brctl setfd br0 15
	brctl stp br0 1
    else
	brctl setfd br0 1
	brctl stp br0 0
    fi
}

stop() {
    :
}

case "$1" in
	start)
	    start
	    ;;

	stop)
	    stop
	    ;;

	restart)
	    stop
	    start
	    ;;

	*)
	    echo $"Usage: $0 {start|stop|restart}"
	    exit 1
esac
