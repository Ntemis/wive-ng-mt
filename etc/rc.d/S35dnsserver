#!/bin/sh

# if app not exist
if [ ! -e /bin/dnsmasq ]; then
    exit 0
fi

# get params
. /etc/scripts/global.sh

LOG="logger -t dnsserver"

start() {
    get_param
    gen_hosts
    gen_resolv
    if [ "$dnsPEnabled" = "1" ] && [ -e $cname ]; then
	# create dir for extended configs
	mkdir -p /etc/dnsmasq.d
	# generate alternative dns config
	gen_diff_dns
	# generate dnssec config
	gen_sec
	# userdnsblock generate
	userblockstart
	$LOG "Starting DNSMASQ"
	dnsmasq -N -c $cachesize --dns-forward-max=$forwardmax --no-poll --all-servers --clear-on-reload --leasefile-ro \
	    -u nobody -g nobody -C "$cname" -r "$fname" -H "$hname" &
    fi
}

gen_hosts() {
  $LOG "Generate /etc/hosts file."
    echo "$HostName" > /etc/hostname
    hostname -F /etc/hostname

    echo "127.0.0.1	localhost"			> /etc/hosts
    echo "$lan_ipaddr	$HostName.lo"			>> /etc/hosts
    echo "$lan_ipaddr	$HostName $HostName.lo"		>> /etc/hosts
    echo "$lan_ipaddr	gateway.lo"			>> /etc/hosts

    if [ "$real_wan_ipaddr" != "" ]; then
	# if dynamic dns and dmz enable - dnsmasq must set DDNS=DMZIP
	# if dynamic dns and dmz disable - dnsmasq must set DDNS=WANIP
	if [ "$DDNS" != "" ]; then
	    if [ "$DMZEnable" = "1" ] && [ "$DMZIPAddress" != "" ]; then
		echo "$DMZIPAddress	$DDNS"		>> /etc/hosts
	    else
		echo "$real_wan_ipaddr	$DDNS"		>> /etc/hosts
	    fi
	fi

	# get domainname from dhcp client
	DHCPCDOMAIN=`cat /etc/resolv.conf | grep domain | awk {' print $2 '}`
	if [ "$DHCPCDOMAIN" != "" ]; then
	    WANDOMAINNAME="$HostName.$DHCPCDOMAIN"
	else
	    WANDOMAINNAME="wan.$HostName.lo"
	fi
	echo "$real_wan_ipaddr	$WANDOMAINNAME"		>> /etc/hosts

	# ipv6 stub
	if [ -d /proc/sys/net/ipv6 ] && [ "$IPv6OpMode" != "0" ]; then
	    echo "::1	localhost"			>> /etc/hosts
	    ip -6 -o addr show scope global | cut -f1 -d/ | awk {' print $4"   ipv6."$2".Wive-NG-MT.lo"'} >> /etc/hosts
	fi
    fi

    echo "# user-defined entries"			>> /etc/hosts
    echo "$dns_local_hosts" | tr ',' '\t' | tr ';' '\n'	>> /etc/hosts

    chmod 644 "$hname" > /dev/null 2>&1
}

gen_resolv() {
    # compose mixed ipv4/6 resolv.conf for easy use
    # filter loopback, sort and uniq
    cat /etc/resolv.conf | grep "nameserver" | grep -vE "nameserver ::1|nameserver 127.0.0.1"  | sort -u > $fname
    chmod 644 "$fname" > /dev/null 2>&1
}

gen_sec() {
    if [ "$dns_sec" = "1" ] && [ -e /etc/trust-anchors.conf ]; then
	echo "conf-file=/etc/trust-anchors.conf" > /etc/dnsmasq.d/sec.conf
	echo "dnssec" >> /etc/dnsmasq.d/sec.conf
    else
	rm -f /etc/dnsmasq.d/sec.conf
    fi
}

gen_diff_dns() {
    if [ "$dns_diffsrv" != "" ]; then
	echo "no-resolv" > /etc/dnsmasq.d/diffsrv.conf
	for srv in $dns_diffsrv ; do
	    echo "server=$srv" >> /etc/dnsmasq.d/diffsrv.conf
	done
    else
	rm -f /etc/dnsmasq.d/diffsrv.conf
    fi
}

get_param() {
    eval `nvram_buf_get 2860 DDNS DMZEnable DMZIPAddress dns_local_hosts dns_adblock dns_userblock dns_sec dns_diffsrv`

    cname="/etc/dnsmasq.conf"
    fname="/tmp/resolvall.conf"
    hname="/etc/hosts"

    cachesize="1024"
    forwardmax="512"

    chmod 644 "$cname" > /dev/null 2>&1
}

reload() {
    get_param
    gen_hosts
    gen_resolv
    is_run=`pidof dnsmasq`
    if [ "$is_run" != "" ]; then
	$LOG "Send HUP to dnsmasq."
	killall -q -SIGHUP dnsmasq
    else
	start
    fi
}

stop() {
    if [ -e /var/run/dnsmasq.pid ]; then
	$LOG "Stopping DNSMASQ"
	pid=`pidof dnsmasq`
	count=0
	# try 3 times for correct shutdown
	while kill "$pid" > /dev/null 2>&1; do
	    pid=`pidof dnsmasq`
	    count="$(($count+1))"
	    if [ "$count" = "3" ] || [ "$pid" = "" ]; then
		break
	    fi
	    usleep 500000
	done
	# if not correct terminate need kill
	if [ "$pid" != "" ]; then
	    kill -SIGKILL "$pid" > /dev/null 2>&1
	    if [ -e /var/run/dnsmasq.pid ]; then
		rm -f /var/run/dnsmasq.pid
	    fi
	fi
    fi
    # remove dnssec config
    rm -f /etc/dnsmasq.d/sec.conf
    # remove different servers config
    rm -f /etc/dnsmasq.d/diffsrv.conf
    # clean userblock list
    userblockstop
}

userblockstart() {
    if [ "$dns_userblock" != "" ]; then
	count=0
	rm -f /etc/dnsmasq.d/userblock.conf
	for url in $dns_userblock ; do
	    if [ "$url" != "" ]; then
		echo "address=/${url}/#" >> /etc/dnsmasq.d/userblock.conf
		count="$(($count+1))"
	    fi
	    $LOG "$count domains user blocked by DNS."
	done
    fi
}

userblockstop() {
    rm -f /etc/dnsmasq.d/userblock.conf
}

adstart() {
    get_param
    # use different thread for auto update and generate rules
    # adblock auto scripts must be start one time and never stopped
    if [ "$dnsPEnabled" = "1" ] && [ "$dns_adblock" = "1" ] && [ ! -e /tmp/adblock_runing ]; then
        # always send to background
        /etc/scripts/config-adblock.sh &
    fi
}

adstop() {
    if [ -e /tmp/adblock_runing ]; then
	pid=`tail -qn1 /tmp/adblock_runing`
	if [ "$pid" != "" ]; then
	    kill "$pid"		> /dev/null 2>&1
	    kill -9 "$pid"	> /dev/null 2>&1
	fi
	rm -f /tmp/adblock_runing
	rm -f /etc/dnsmasq.d/ads.conf
    fi
}

case "$1" in
        start)
            start
            ;;

        stop)
            stop
            ;;

        reload)
            reload
            ;;

        adstart)
            adstart
            ;;

        adstop)
            adstop
            ;;

        adrestart)
	    adstop
            adstart
            ;;

        restart)
            stop
            start
            ;;

        *)
            echo $"Usage: $0 {start|stop|reload|restart|adstart|adrestart}"
esac
