#!/bin/sh

# Modified by N.Leiten and adopted for Wive-firmware, renamed in "fs"
# "flash" is a part of midge - mini distribuition for adm5120 based routers.
# Full rewrite for Wive-NG (RT3050/RT3052/RT3662/RT3883/MT762x) by Evgeniy Manachkin.
# Copyright (C) 2004-2005 by Vladislav Moskovets <midge at vlad.org.ua>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU GPL v2 or later.

# include kernel config
. /etc/scripts/config.sh

# mtd dev for rfs
MTDRWFS=`cat /proc/mtd | grep "RW-FS" | awk {' print "/dev/"$1 '} | cut -f -1 -d:`
# maximum size rwfs to write
RWFSSIZEHEX="0x`cat /proc/mtd | grep "RW-FS" | awk {' print $2 '}`"
TGZfs_size=`echo $(($RWFSSIZEHEX))`

# backuped rf calibs
RFCONFIGS=/etc/Wireless/rfbackup.bin

get_size() {
  ls -sk "$1" | sed -e "s/ \+/ /g" | cut -d" " -f2
}

usage() {
  echo "Usage: $0 | save | load | restore | nvramreset | burnrf | change_mac | fullreset |" >&2
  echo "____________________________________________________________________________" >&2
  echo " " >&2
  echo "save - Save rwfs to mtd." >&2
  echo "load - Load rwfs from mtd." >&2
  echo "restore - Crash RW-FS and reboot for restore clean rwfs." >&2
  echo "nvramreset - Nvram clear and load default config." >&2
  echo "burnrf - Burn RF config template to Factory." >&2
  echo "change_mac - Change/clone wan mac adreess." >&2
  echo "factory_mac - Set mac in nvram from factory." >&2
  echo "fullreset - 1. Full zero write in config,factory and rwfs particions." >&2
  echo "	    2. Reboot device." >&2
  echo "	    3. Write temlate for RF config (Factory) to flash." >&2
  echo "	    4. Load and write default setting (Config) to flash." >&2
  echo "	    5. Generate new macs, keys and others." >&2
  echo "	    6. Generate new mac and flash it." >&2
  echo "____________________________________________________________________________" >&2
 exit 1
}

sys_led_blink() {
    # blink sys/vpn leds
    if [ -e /bin/gpio ]; then
	if [ "$CONFIG_RALINK_GPIO_SYS_LED" != "" ]; then
	    gpio l "$CONFIG_RALINK_GPIO_SYS_LED" 1 1 0 0 1000
	fi
    fi
}

load() {
    if [ "$MTDRWFS" = "" ]; then
	echo "RWFS Not Found!"
	exit 1
    else
	echo "Loading RWFS"
	local dst=/
	bzcat $MTDRWFS | tar xf - -C $dst>>/dev/null
    fi
}

save() {
    echo "Save curent date and current time to rwfs"
    date +%Y%m%d%H%M > /etc/compile-date
    echo "Compress config files"
    tmp="/tmp/tgzfs"
    tar cf - --exclude ads.conf /etc | bzip2 -9 > $tmp
    if [ "$(get_size $tmp)" -lt $(($TGZfs_size/1024)) ]; then
	echo "Write RW-FS to flash ($(get_size $tmp)kB of $(($TGZfs_size/1024))kB)"
	mtd_write write $tmp RW-FS
	echo "Config saved. OK."
    else
	echo "Error: File $tmp too big: $(get_size $tmp)k"
    fi
    rm -f $tmp
}

restore() {
    echo "Clear rwfs."
    mtd_write erase RW-FS
}

nvramreset() {
    # cleanup with skip clean macs and is_wive and checkmacs flags
    echo "Clear nvram and load user defaults."
    sys_led_blink
    nvram_default 2860
}

burnrf() {
    echo "Erase wifi Factory part and write $RFCONFIGS as defaults"
    mtd_write erase Factory
    mtd_write write $RFCONFIGS Factory
}

fullreset() {
    echo "Stop services and unload modules."
    killall -q nginx
    killall -q -SIGKILL nginx
    sys_led_blink
    unload_all.sh
    echo "Erase rwfs,nvram,factory (rebuild after reboot)."
    mtd_write erase Factory
    mtd_write erase Config
    mtd_write erase RW-FS
    echo "Now reboot"
    reboot
}

change_mac() {
    echo "Set WAN MAC = $1"
    sys_led_blink
    nvram_set 2860 CHECKMAC NO
    FWMAC=`echo $1 | sed "s/:/ /g"`
    eth_mac w wan $FWMAC
    nvram_set 2860 WAN_MAC_ADDR "$1"
    echo "Now reboot"
    reboot
}

factory_mac() {
    echo "Set nvram mac adresses from factory"
    MACWAN=`eth_mac r wan | grep -E "^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$"`
    if [ "$MACWAN" != "" ]; then
	echo "Set WAN   MAC - $MACWAN"
	nvram_set 2860 WAN_MAC_ADDR   "$MACWAN"
    else
	echo "WANMAC corrupted."
    fi
    MACLAN=`eth_mac r lan | grep -E "^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$"`
    if [ "$MACLAN" != "" ]; then
	echo "Set LAN   MAC - $MACLAN"
	nvram_set 2860 LAN_MAC_ADDR   "$MACLAN"
    else
	echo "LANMAC corrupted."
    fi
    if [ "$CONFIG_RT_FIRST_IF_NONE" = "" ]; then
	MACWLAN=`eth_mac r wlan | grep -E "^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$"`
	if [ "$MACWLAN" != "" ]; then
	    echo "Set WLAN1 MAC - $MACWLAN"
	    nvram_set 2860 WLAN_MAC_ADDR  "$MACWLAN"
	else
	    echo "WLANMAC1 corrupted."
	fi
    fi
    if [ "$CONFIG_RT_SECOND_IF_NONE" = "" ]; then
	MACWLAN2=`eth_mac r wlan2 | grep -E "^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$"`
	if [ "$MACWLAN2" != "" ]; then
	    echo "Set WLAN2 MAC - $MACWLAN2"
	    nvram_set 2860 WLAN2_MAC_ADDR "$MACWLAN2"
	else
	    echo "WLANMAC2 corrupted."
	fi
    fi
    nvram_set 2860 CHECKMAC YES
}

main() {
  local cmd="$1"
  [ -z "$cmd" ] && usage
  shift
  case "$cmd" in
    load)
      load $@
      ;;
    save)
      save $@
      ;;
    restore)
      restore $@
      ;;
    nvramreset)
      nvramreset $@
      ;;
    burnrf)
      burnrf $@
      ;;
    fullreset)
      fullreset $@
      ;;
    change_mac)
      change_mac $@
      ;;
    factory_mac)
      factory_mac $@
      ;;
    *)
      usage $@
      ;;
  esac
}

main $@
