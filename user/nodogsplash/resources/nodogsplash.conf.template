# FirewallRuleSet: authenticated-users
#
# Control access for users after authentication.
# These rules are inserted at the beginning of the
# FORWARD chain of the router's filter table, and
# apply to packets that have come in to the router
# over the GatewayInterface from MAC addresses that
# have authenticated with Nodogsplash, and that are
# destined to be routed through the router.  The rules are
# considered in order, and the first rule that matches
# a packet applies to it.
# If there are any rules in this ruleset, an authenticated
# packet that does not match any rule is rejected.
# N.B.: This ruleset is completely independent of
# the preauthenticated-users ruleset.
#
FirewallRuleSet authenticated-users {

 # You may want to open access to a machine on a local
 # subnet that is otherwise blocked (for example, to
 # serve a redirect page; see RedirectURL).  If so,
 # allow that explicitly here, e.g:
 #  FirewallRule allow tcp port 80 to 192.168.254.254

 # Your router may have several interfaces, and you
 # probably want to keep them private from the GatewayInterface.
 # If so, you should block the entire subnets on those interfaces, e.g.:
    #FirewallRule block to 192.168.0.0/16
    #FirewallRule block to 10.0.0.0/8

 # Typical ports you will probably want to open up include
 # 53 udp and tcp for DNS,
 # 80 for http,
 # 443 for https,
 # 22 for ssh:
    #FirewallRule allow tcp port 53
    #FirewallRule allow udp port 53
    #FirewallRule allow tcp port 80
    #FirewallRule allow tcp port 443
    #FirewallRule allow tcp port 22
    FirewallRule allow all

}
# end FirewallRuleSet authenticated-users

# FirewallRuleSet: preauthenticated-users
#
# Control access for users before authentication.
# These rules are inserted in the PREROUTING chain
# of the router's nat table, and in the
# FORWARD chain of the router's filter table.
# These rules apply to packets that have come in to the
# router over the GatewayInterface from MAC addresses that
# are not on the BlockedMACList or TrustedMACList,
# are *not* authenticated with Nodogsplash.  The rules are
# considered in order, and the first rule that matches
# a packet applies to it. A packet that does not match
# any rule here is rejected.
# N.B.: This ruleset is completely independent of
# the authenticated-users and users-to-router rulesets.
#
FirewallRuleSet preauthenticated-users {
 # For preauthenticated users to resolve IP addresses in their initial
 # request not using the router itself as a DNS server,
 # you probably want to allow port 53 udp and tcp for DNS.
    FirewallRule allow tcp port 53
    FirewallRule allow udp port 53
 # For splash page content not hosted on the router, you
 # will want to allow port 80 tcp to the remote host here.
 # Doing so circumvents the usual capture and redirect of
 # any port 80 request to this remote host.
 # Note that the remote host's numerical IP address must be known
 # and used here.
 #    FirewallRule allow tcp port 80 to 123.321.123.321
}
# end FirewallRuleSet preauthenticated-users

# FirewallRuleSet: users-to-router
#
# Control access to the router itself from the GatewayInterface.
# These rules are inserted at the beginning of the
# INPUT chain of the router's filter table, and
# apply to packets that have come in to the router
# over the GatewayInterface from MAC addresses that
# are not on the TrustedMACList, and are destined for
# the router itself.  The rules are
# considered in order, and the first rule that matches
# a packet applies to it.
# If there are any rules in this ruleset, a
# packet that does not match any rule is rejected.
#
FirewallRuleSet users-to-router {
 # Nodogsplash automatically allows tcp to GatewayPort,
 # at GatewayAddress, to serve the splash page.
 # However you may want to open up other ports, e.g.
 # 53 for DNS and 67 for DHCP if the router itself is
 # providing these services.
    FirewallRule allow udp port 53
    FirewallRule allow tcp port 53
    FirewallRule allow udp port 67
 # You may want to allow ssh, http, and https to the router
 # for administration from the GatewayInterface.  If not,
 # comment these out.
    FirewallRule allow tcp port 22
    FirewallRule allow tcp port 80
    FirewallRule allow tcp port 443
}
# end FirewallRuleSet users-to-router

# EmptyRuleSetPolicy directives
# The FirewallRuleSets that NoDogSplash permits are:
#
# authenticated-users
# preauthenticated-users
# users-to-router
# trusted-users
# trusted-users-to-router
#
# For each of these, an EmptyRuleSetPolicy can be specified.
# An EmptyRuleSet policy applies to a FirewallRuleSet if the
# FirewallRuleSet is missing from this configuration file,
# or if it exists but contains no FirewallRules.
#
# The possible values of an EmptyRuleSetPolicy are:
# allow  -- packets are accepted
# block  -- packets are rejected
# passthrough -- packets are passed through to pre-existing firewall rules
#
# Default EmptyRuleSetPolicies are set as follows:
# EmptyRuleSetPolicy authenticated-users passthrough
# EmptyRuleSetPolicy preauthenticated-users block
# EmptyRuleSetPolicy users-to-router block
# EmptyRuleSetPolicy trusted-users allow
# EmptyRuleSetPolicy trusted-users-to-router allow

# Parameter: BinVoucher
# Default: None
#
# Enable Voucher Support.
# If set, an alphanumeric voucher HTTP parameter is accepted
# and passed to a command line call along with the clients MAC:
#
# $<BinVoucher> auth_voucher <mac> <voucher>
#
# BinVoucher must point to a program that will be called as described above.
# The call is expected to output the number of seconds the client
# is to be authenticated. Zero or negative seconds will cause the
# authentification request to be rejected.
# The output may contain a user specific download and upload limit in KBit/s:
# <seconds> <upload> <download>
#
# BinVoucher "/bin/myauth"

# Parameter: ForceVoucher
# Default: no
#
# Force the use of a voucher. Authentification is not possible without voucher.
#
# ForceVoucher no

# Parameter: EnablePreAuth
# Default: no
#
# Enable pre-authentication support.
# Pass the MAC of a client to a command line call before the splash page
# would be send:
#
# $<BinVoucher> auth_status <mac>
#
# The call is expected to output the number of seconds the client
# is to be authenticated. Zero or negative seconds will cause the
# splash page to be displayed.
# The output may contain a user specific download and upload limit in KBit/s:
# <seconds> <download> <upload>
#
# EnablePreAuth no
