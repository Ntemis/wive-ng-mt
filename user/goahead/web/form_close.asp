<!DOCTYPE html>
<html>
<!-- Copyright (c) Ralink (2004-2010) Tech Inc. All Rights Reserved. -->
<!-- Copyright (c) Wive-NG project (2010-2016) http://wive-ng.sf.net. All Rights Reserved. -->
<head>
<title>Form submitted</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0">
<meta http-equiv="Pragma" content="no-cache">
</head>
<body>
<h1>Form has been submitted</h1>
<script language="JavaScript" type="text/javascript">
	window.close();
</script>
</html>
