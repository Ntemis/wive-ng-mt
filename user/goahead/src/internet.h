/*
 *	internet.h -- Internet Configuration Header
 *
 *	Copyright (c) Ralink Technology Corporation All Rights Reserved.
 *
 *	$Id: internet.h,v 1.8 2008-03-17 07:47:16 yy Exp $
 */

#ifndef _h_INTERNET
#define _h_INTERNET 1

void formDefineInternet(void);
void initInternet(void);
#endif
