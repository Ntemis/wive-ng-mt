/*
 *	firewall.h -- Firewall Configuration Header 
 *
 *	Copyright (c) Ralink Technology Corporation All Rights Reserved.
 *
 *	$Id: firewall.h,v 1.5 2008-04-14 09:26:00 yy Exp $
 */

#ifndef _h_FIREWALL
#define _h_FIREWALL 1

void formDefineFirewall(void);
void firewall_rebuild(void);
void firewall_rebuild_etc(void);
#endif
