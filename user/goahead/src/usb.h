/*
 *	usb.h -- USB Application Settings
 *
 *	Copyright (c) Ralink Technology Corporation All Rights Reserved.
 *
 *	$Id: usb.h,v 1.2 2009-01-24 10:07:16 chhung Exp $
 */

#ifndef _h_USB
#define _h_USB 1

void formDefineUSB(void);
void formDefineSTORAGE(void);
int initUSB(void);
#endif
